//
//  TextViewCustom.swift
//  ISO
//
//  Created by Mac Other on 25/07/2022.
//
import UIKit

class TextViewCustom: UITextView, UITextViewDelegate {

    
    private var event: (()->())!

    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setOb(handleNext: @escaping (()->())) {
        self.event = handleNext
    }

    override func awakeFromNib() {
        delegate = self
        super.awakeFromNib()
        layer.borderWidth = 1
        layer.cornerRadius = 8
        colorBoder(grayColor: true)
        keyboardType = .numberPad
    }

    func colorBoder(grayColor: Bool) {
        layer.borderColor =  grayColor ? UIColor.gray.cgColor : UIColor.red.cgColor
    }

    func textViewDidChangeSelection(_ textView: UITextView) {
        switch textView.text.count {
        case 0:
            colorBoder(grayColor: true)
        default:
            colorBoder(grayColor: false)
        }
    }

    func textViewDidChange(_ textView: UITextView) {
        let first = textView.text.last?.description
        textView.text = first
        event?()
    }
}
