//
//  ViewController.swift
//  ISO
//
//  Created by Mac Other on 25/07/2022.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var input1: TextViewCustom!
    @IBOutlet weak var input2: TextViewCustom!
    @IBOutlet weak var input3: TextViewCustom!
    @IBOutlet weak var input4: TextViewCustom!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configButton()
        self.setObTextInput()
    }

    private func configButton() {
        btnVerify.layer.cornerRadius = 8
    }

    private func setObTextInput() {
        input1.setOb {
            self.input()
        }
        input2.setOb {
            self.input()
        }
        input3.setOb {
            self.input()
        }
        input4.setOb {
            self.input()
        }
    }
    
    @IBAction func reset(_ sender: Any) {
        input1.text = nil
        input2.text = nil
        input3.text = nil
        input4.text = nil
        input1.becomeFirstResponder()
        btnVerify.isHidden = true
    }

    private func input() {
        let notEmpty = input1.text.count == 1 && input2.text.count == 1 && input3.text.count == 1 && input4.text.count == 1
        if notEmpty {
            self.btnVerify.isHidden = false
        } else {
            self.btnVerify.isHidden = true
        }
    }
}
